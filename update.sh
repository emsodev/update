#/bin/bash

# download_url="https://bitbucket.org/emsodev/update/downloads/emsodev.tar.gz"

git_repo_url="https://bitbucket.org/emsodev/acquisition"

readonly backup_folder="/var/emsodev-backups"
readonly emsodev_path="/opt/emsodev/"
readonly temp_files="/var/tmp"
readonly new_version="new-version-emsodev.tar.gz"
readonly versionfile="/opt/emsodev/conf/version.txt"
readonly old_emsodev_path="/opt/.emsodev.old/"

#---- Functions ----#
show_usage(){
    echo "This script downloads the last version of the EMSODEV Acquisition"
    echo "software and installs it."
    echo ""

    echo "Options:"
    echo "    -u        specifies acquisition user owner (default \"admin\")"
    echo "    -U        specifies another URL to get the new package"
    echo "    -h        shows this help"

    echo ""
    exit
}


#-------------------------------------------------------------#
#------------------ PROGRAM STARTS HERE ----------------------#
#-------------------------------------------------------------#

#---- Check Arguments ----#
while getopts u:U:h option; do
    case "${option}" 
    in
        u) acquisition_user=${OPTARG};;       
        U) download_url=${OPTARG};;
        h) show_usage;;
    esac
done

set -o errexit  # Exits script on error
set -o nounset  # Detects uninitialized variables on the script and exits with error


#---- Check if sudo ----#
# id -u returns the user ID (root ID=0)
if [ $(id -u )  != 0 ]; then
    echo "Please run as root"
    exit
fi

if [ $# != 1 ]; then
    echo "Usage: $0 <username>"
    exit
fi
acquisition_user=$1


# check if temp folder file exists #

if [ ! -d temp_files] ; then
    echo "error, $temp_files folder does not exist!"
    exit
fi


#---- Ask to user ----#
echo "to show options  ${0} -h"
echo "\n----------------------------"
echo "Updating EMSODEV Acquisition"
echo "----------------------------\n"
echo "EMSODEV acquisition will be updated to its latest version."
echo "The owner of the acquisition will be set to user: $acquisition_user"
echo "The new acquisition will be downloaded from:"
echo "$git_repo_url"
read -p "Do you want to update EMSODEV acquisition (y/n)?" choice
case "$choice" in 
  y|Y ) echo "yes" ;;
  n|N ) echo "aborting" ; exit ;;
  * ) echo "invalid option, aborting" ; exit ;;
esac

# check if there's an existing acquisition version (/opt/emsodev)
if [ -d $emsodev_path ]; then
    # create a backup
    if [ -f $versionfile ]; then
        oldversion=$(cat $versionfile | cut -d: -f2)
        echo "old version: $oldversion"
    fi
    
    # Stop aqcuisition
    if [ -e     ${emsodev_path}/acquisition/acquisition_manager.sh ] ; then
        ${emsodev_path}/acquisition/acquisition_manager.sh stop all 
    fi
    
    printf "Creating a backup of $emsodev_path..."    
    mkdir -p ${backup_folder}
    # create the backup 
    backup_file="${backup_folder}/$(date +%Y%d%m_%H%M%S).tar.gz"
    tar --absolute-names -zcf "${backup_file}" ${emsodev_path}
    echo "success"
    echo "new backup file ${backup_file}"
    
    # copying *.conf files



    if [ -e ${emsodev_path}/conf/emsodev.conf ]; then 
        datetime=$(date +%Y%m%d_%H%M%S)
        conf_files_path="${backup_folder}/${datetime}"      
        echo "copying conf files to ${conf_files_path}..."
        mkdir -p $conf_files_path
        cp ${emsodev_path}/conf/*.conf ${conf_files_path}
    fi
    
    mv ${emsodev_path} $old_emsodev_path
    
fi

mkdir -p ${emsodev_path}
chown -R ${acquisition_user}:${acquisition_user} ${emsodev_path}
cd ${emsodev_path}

git clone $git_repo_url ${emsodev_path}

chown -R ${acquisition_user}:${acquisition_user} ${emsodev_path}

echo "\nEMSODEV Acquisition successfully updated!"

